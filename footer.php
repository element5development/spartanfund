
</main>
<a href="#0" class="cd-top">Top</a>

<footer class="site-footer">
	<section class="invest">
		<svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 160 160"><defs><style>.cls-1{fill:#fff;}</style></defs><path class="cls-1" d="M69.39 61.58l-8-14.65a35.55 35.55 0 0 0-6 3.72l10.81 12.8a19.77 19.77 0 0 0 3.19-1.87zM76 58.92l-4.56-16.45a37.23 37.23 0 0 0-6.8 2.76l7.54 15C75.63 59 76 58.88 76 58.92zM42.44 66.05l13.49 6.8s2.19-2.79 2.23-2.76l-12.5-9.41a25.25 25.25 0 0 0-3.22 5.37z"/><path class="cls-1" d="M31.89 83.57C33.08 67 44.56 51.49 58.8 42.79c21.35-12.65 46.77-11.38 66.14-2.46 4-8.31 11.74-25 11.74-25C85.1-12.21 33.31 21 21.34 49c-13.11 30.68.45 55.15 2.17 65 4.32 24.89-6.67 30.31-8.25 32.29 7.12-.4 11.38-5.54 14.93-11.48a41.6 41.6 0 0 0 4.53-17c1.59-11.04-3.22-23.16-2.83-34.24z"/><path class="cls-1" d="M87.16 38.86l-.54 18.06c.76 0 1.9.18 1.92.1a19.37 19.37 0 0 1 6.18-11.56c5.87-5 13.58-4.52 13.5-4.57a57.36 57.36 0 0 0-21.06-2.03zM63.37 65.2L52.53 52.88a32 32 0 0 0-4.67 4.89l12.73 10zM82.48 57.23l.43-17.84c-.14.14-4.14.75-7.54 1.7L79 58c.12.05 3.44-.76 3.48-.77zM37.77 79.43c-1.17 7.44-1.25 14.34-.62 18 0 0 1.1-6.3 4.71-9.87a12.81 12.81 0 0 1 8.57-3.67 22.38 22.38 0 0 0 .62-2.44zM127.1 86.09c-4.9-12-15-20.16-28.41-22.53-7.13-1.56-14.64-1.56-21.34.88C67.46 68 61.06 75.09 56.82 84.71c-7.08 16 2.92 28.26 2.92 41.31.4 4.74-2.35 14.26-2 14.2 12.83-1.77 21.5-5.86 32.58-7.83 7.54 6.58 33.32 21.75 39 22.53.39.05-6.53-22.46-5.15-47.27-6.52 1-14.94-1.72-19.84-7.11 6.51-9.35 30.13-8.32 26.7 16.66-.05.39 14 2.76 13.72 2.45-8.83-10.78-12.51-20.95-17.65-33.56zM38.62 75.4l14 2.65c.39-1 .86-1.86 1.43-3l-13.62-5.28a28.23 28.23 0 0 0-1.81 5.63zM134.38 148.92a3.1 3.1 0 0 1 1.49.39 2.76 2.76 0 0 1 1.14 1.13 3.06 3.06 0 0 1 .4 1.52 3 3 0 0 1-.4 1.51 2.77 2.77 0 0 1-1.12 1.13 3 3 0 0 1-1.51.4 3.09 3.09 0 0 1-1.52-.4 2.83 2.83 0 0 1-1.12-1.13 3 3 0 0 1-.41-1.51 3.07 3.07 0 0 1 .41-1.52 2.88 2.88 0 0 1 1.14-1.13 3.17 3.17 0 0 1 1.5-.39zm0 .5a2.57 2.57 0 0 0-1.25.33 2.32 2.32 0 0 0-.95.94 2.55 2.55 0 0 0-.34 1.27 2.58 2.58 0 0 0 .33 1.26 2.39 2.39 0 0 0 1 .94 2.59 2.59 0 0 0 1.26.34 2.65 2.65 0 0 0 1.26-.34 2.43 2.43 0 0 0 .94-.94 2.58 2.58 0 0 0 .33-1.26 2.55 2.55 0 0 0-.34-1.27 2.32 2.32 0 0 0-.95-.94 2.56 2.56 0 0 0-1.29-.33zm-1.38 4.22v-3.27h1.13a2.75 2.75 0 0 1 .83.09.81.81 0 0 1 .41.32.86.86 0 0 1 .16.48.87.87 0 0 1-.26.62 1 1 0 0 1-.68.3 1 1 0 0 1 .28.17 4 4 0 0 1 .48.65l.4.64h-.64l-.29-.51a2.78 2.78 0 0 0-.56-.77.68.68 0 0 0-.42-.11h-.31v1.39zm.53-1.84h.64a1 1 0 0 0 .63-.14.45.45 0 0 0 .17-.36.41.41 0 0 0-.09-.26.43.43 0 0 0-.22-.17 1.54 1.54 0 0 0-.53-.06h-.6z"/></svg>
		<h2>Invest In Champions.</h2>
		<p>Make Your Gift Today!</p>
		<a target="_blank" href="https://michiganstate.donornetpac.com/MSU/GiveNow/" class="button_link">Donate Now</a>
	</section>
	<section>
		<?php dynamic_sidebar( 'footer-col-one' ); ?>
	</section>
	<section>
		<?php dynamic_sidebar( 'footer-col-two' ); ?>
	</section>
	<section>
		<?php dynamic_sidebar( 'footer-col-three' ); ?>
	</section>
	<section>
		<?php dynamic_sidebar( 'footer-col-four' ); ?>
	</section>
	<section>
		<?php dynamic_sidebar( 'footer-col-five' ); ?>
	</section>
	<section>
		<?php dynamic_sidebar( 'footer-col-six' ); ?>
	</section>
	<section>
		<?php dynamic_sidebar( 'footer-col-seven' ); ?>
	</section>
	<section class="copyright">
		<p>©Copyright 2019. Spartan Fund. All Rights Reserved. This site is compliant with the CCPA and GDPR. <a href="/privacy-policy/">Privacy Policy</a> | <button class="cookie-open">Cookie Consent</button></p>
	</section>
</footer>

<?php wp_footer(); ?>

<!-- JS -->
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/retina.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.meanmenu.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.localscroll-1.2.7-min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.scrollTo-1.4.2-min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/standalone.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/ihavecookies.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/main.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/functions.js"></script>

</body>
</html>
