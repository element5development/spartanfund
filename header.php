<!doctype html>

<!--[if lt IE 7 ]> <html class="ie ie6 ie-lt10 ie-lt9 ie-lt8 ie-lt7 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7 ]>    <html class="ie ie7 ie-lt10 ie-lt9 ie-lt8 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8 ]>    <html class="ie ie8 ie-lt10 ie-lt9 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 9 ]>    <html class="ie ie9 ie-lt10 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 9]><!--><html class="no-js" <?php language_attributes(); ?>><!--<![endif]-->
<!-- the "no-js" class is for Modernizr. --> 

<head id="<?php echo of_get_option('meta_headid'); ?>" data-template-set="html5-reset-wordpress-theme">

  <meta charset="<?php bloginfo('charset'); ?>">
  <!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->
  <!--[if IE ]>
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <![endif]-->
  <?php
    if (is_search())
      echo '<meta name="robots" content="noindex, nofollow" />';
  ?>
  <title><?php wp_title( '|', true, 'right' ); ?></title>
  <meta name="title" content="<?php wp_title( '|', true, 'right' ); ?>">

  <?php
    if (true == of_get_option('meta_author'))
      echo '<meta name="author" content="' . of_get_option("meta_author") . '" />';

    if (true == of_get_option('meta_google'))
      echo '<meta name="google-site-verification" content="' . of_get_option("meta_google") . '" />';
  ?>
  <meta name="Copyright" content="Copyright &copy; <?php bloginfo('name'); ?> <?php echo date('Y'); ?>. All Rights Reserved.">

  <?php
    /*
      j.mp/mobileviewport & davidbcalhoun.com/2010/viewport-metatag 
       - device-width : Occupy full width of the screen in its current orientation
       - initial-scale = 1.0 retains dimensions instead of zooming out if page height > device height
       - maximum-scale = 1.0 retains dimensions instead of zooming in if page width < device width
    */
    if (true == of_get_option('meta_viewport'))
      echo '<meta name="viewport" content="' . of_get_option("meta_viewport") . '" />';
  
  ?>

  <!--Apple Mobile Web App Metadata-->  
  <meta name="apple-mobile-web-app-capable" content="yes" />
  <meta name="apple-mobile-web-app-status-bar-style" content="black" />
  <meta name="viewport" content="width=device-width, initial-scale=1"> 
  <link rel="apple-touch-startup-image" media="(max-device-width: 480px) and not (-webkit-min-device-pixel-ratio: 2)" href="<?php echo get_template_directory_uri(); ?>/images/startup@2x.png" />
  <link rel="apple-touch-startup-image" media="(max-device-width: 480px) and (-webkit-min-device-pixel-ratio: 2)" href="<?php echo get_template_directory_uri(); ?>/images/startup@2x.png" />
  <link rel="apple-touch-startup-image" media="(max-device-width: 568px) and (-webkit-min-device-pixel-ratio: 2)" href="<?php echo get_template_directory_uri(); ?>/images/startup5@2x.png" />
    <link rel="apple-touch-startup-image" media="(device-width: 768px) and (device-height: 1024px) and (orientation:landscape) and (-webkit-min-device-pixel-ratio: 2)" href="<?php echo get_template_directory_uri(); ?>/images/startup-ipad-landscape@2x.png"  />
    <link rel="apple-touch-startup-image"  media="(device-width: 768px) and (device-height: 1024px) and (orientation:portrait) and (-webkit-min-device-pixel-ratio: 2)" href="<?php echo get_template_directory_uri(); ?>/images/startup-ipad-portrait@2x.png"  />
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/images/touch-icon-iphone.png" />

  <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.png">   

  <!-- CSS -->
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/reset.css" />
  <link rel="stylesheet" href="<?php echo get_stylesheet_uri(); ?>" />

  <script type="text/javascript" src="//use.typekit.net/vgw4iru.js"></script>
  <script type="text/javascript">try{Typekit.load();}catch(e){}</script>

  <!-- This is an un-minified, complete version of Modernizr. 
     Before you move to production, you should generate a custom build that only has the detects you need. -->
  <script src="<?php echo get_template_directory_uri(); ?>/js/modernizr-2.6.2.dev.js"></script>

  <link rel="profile" href="http://gmpg.org/xfn/11" />
  <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

  <?php wp_head(); ?>
	
	<?php if(isset($_COOKIE['cookieControlPrefs']) && $_COOKIE['cookieControlPrefs'] == '[\"analytics\"]' || $_COOKIE['cookieControlPrefs'] == '[\"analytics\",\"marketing\"]' ) { ?>
		<?php
		/*----------------------------------------------------------------*\
		|
		|	Drop Google Tag Manager head code here
		| Hot Jar tracking will be added within Google Tag Manager
		|
		\*----------------------------------------------------------------*/
		?>
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-52099549-1"></script>
		<script>
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());

			gtag('config', 'UA-52099549-1');
		</script>
  
	<?php } ?>
	<?php if(isset($_COOKIE['cookieControlPrefs']) && $_COOKIE['cookieControlPrefs'] == '[\"marketing\"]' || $_COOKIE['cookieControlPrefs'] == '[\"analytics\",\"marketing\"]' ) { ?>
		<?php
		/*----------------------------------------------------------------*\
		|
		|	Drop any marketing tracking pixels here
		| These are related to paid advertising like Google Ads
		| and Facebook ad pixels.
		|
		\*----------------------------------------------------------------*/
		?>
		<!--CART ABANDONMENT TRACKING REQUESTED TO BE PLACE BY CLIENT ON 3/2 VIA ZENDESK #34421 -->
		<?php if ( is_front_page() ) { ?>
			<script>
				document.write("<img height='0' width='0' alt='' src='http://athletics.msuspartans.com/r/"
				+  Math.random().toString() + "?tagid=6c22bf4c&jobid=6a438fdd&ibl'/>");
			</script>
			<noscript><img height='0' width='0' alt='' src='http://athletics.msuspartans.com/r/?tagid=6c22bf4c&jobid=6a438fdd&ibl'/></noscript>
		<?php } ?>

		<?php if ( is_page(28) ) { ?>
			<script>
			document.write("<img height='0' width='0' alt='' src='http://athletics.msuspartans.com/r/"
			+  Math.random().toString() + "?tagid=6bf42660&jobid=6a438fdd&ibl'/>");
			</script>
			<noscript><img height='0' width='0' alt='' src='http://athletics.msuspartans.com/r/?tagid=6bf42660&jobid=6a438fdd&ibl'/></noscript>
		<?php } ?>
  
	<?php } ?>

</head>

<body <?php body_class(); ?>>

	<?php if(isset($_COOKIE['cookieControlPrefs']) && $_COOKIE['cookieControlPrefs'] == '[\"analytics\"]' || $_COOKIE['cookieControlPrefs'] == '[\"analytics\",\"marketing\"]' ) { ?>
		<?php
		/*----------------------------------------------------------------*\
		|
		|	Drop Google Tag Manager body code here
		|
		\*----------------------------------------------------------------*/
		?>
	<?php } ?>

		<a href="#pagecontent" class="button_link skip" aria-label="skip to main content">Skip to main content</a>

    <header id="header" role="banner" class="nav-down">
      <a href="<?php echo home_url(); ?>" class="logo">Spartan Fund</a>

      <nav id="nav" role="navigation">
        <?php wp_nav_menu( array('menu' => 'Main Menu') ); ?>
      </nav>
    </header>

    <div class="logobg">
      <a href="<?php echo home_url(); ?>" class="logo-mobile">Spartan Fund</a>
    </div>

    <main class="contents">
    

