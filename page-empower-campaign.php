<?php
/**
 * Template Name: Empower Extraordinary Campaign
 */
 get_header(); ?>

<div id="empower-lp">

	<section id="slide1" class="slide">
			<article class="intro">
				<h1>Empower<strong>Extraordinary</strong></h1>
				<h2>The Campaign <span>for</span> <strong>Michigan State University</strong></h2>
				<?php the_field('content_block_1'); ?>
			</article>

			<img src="<?php bloginfo('template_url'); ?>/images/empower/laurel-wreath.png" class="wreath" alt="MSU Laurel Wreath" />
			<div class="overlay_white"></div>

			<div class="scrolldown"><a href="#start">Scroll Down to See More</a></div>

			<div id="start"></div>
			
			<article class="info-1">
				<?php the_field('content_block_2'); ?>
			</article>

			<article class="info-2">
				<?php the_field('content_block_3'); ?>
			</article>	

			<article class="info-3">
				<div class="entry">
					<iframe width="560" height="315" src="https://www.youtube.com/embed/NS8Jz-EqctE?rel=0" frameborder="0" allowfullscreen></iframe>
				</div>
			</article>
			
			<article class="info-4">
				<h2>$262 Million <span>Campaign Goal</span></h2>
				<div class="progresscontainer bar1">
					<?php $campaigngoal = get_field('current_progress_campaign_goal'); ?>
					<h3>Our Current Progress</h3>
					<progress max="262" value="<?php echo $campaigngoal; ?>"></progress>
					<span class="min">$0</span>
					<span class="tooltip">$<?php echo $campaigngoal; ?>M</span>
					<span class="max">$262M</span>
				</div>
			</article>

	</section>

	<section id="slide2" class="slide">
		<article>
			<h2>Our Campaign <strong>Vision</strong></h2>
			<img src="<?php bloginfo('template_url'); ?>/images/empower/vision-92m@2x.png" alt="$92 Million Facility Projects" />
			<img src="<?php bloginfo('template_url'); ?>/images/empower/vision-50m@2x.png" alt="$50 Million Endowments" />
			<img src="<?php bloginfo('template_url'); ?>/images/empower/vision-120m@2x.png" alt="$120 Million Annual and Premium Seating Gifts" />
		</article>
	</section>

	<section id="slide3" class="slide">
		<article>
			<?php the_field('facility_projects'); ?>
				<div class="progresscontainer bar2">
					<h3>Our Current Progress</h3>
					<?php $facilities = get_field('current_progress_facilities'); ?>
					<progress max="92" value="<?php echo $facilities; ?>"></progress>
					<span class="min">$0</span>
					<span class="tooltip">$<?php echo $facilities; ?>M</span>
					<span class="max">$92M</span>
				</div>
		</article>
	</section>

	<section id="slide4" class="slide">
		<article>
			<?php the_field('endowments'); ?>
				<div class="progresscontainer bar3">
					<h3>Our Current Progress</h3>
					<?php $endowments = get_field('current_progress_endowments'); ?>
					<progress max="50" value="<?php echo $endowments; ?>"></progress>
					<span class="min">$0</span>
					<span class="tooltip">$<?php echo $endowments; ?>M</span>
					<span class="max">$50M</span>
				</div>
		</article>
	</section>

	<section id="slide5" class="slide">
		<article>
			<?php the_field('annual_premium_seating_gifts'); ?>
			<div class="progresscontainer bar4">
				<h3>Our Current Progress</h3>
				<?php $gifts = get_field('current_progress_gifts'); ?>
				<progress max="120" value="<?php echo $gifts; ?>"></progress>
				<span class="min">$0</span>
				<span class="tooltip">$<?php echo $gifts; ?>M</span>
				<span class="max">$120M</span>
			</div>
		</article>
	</section>

	<section id="slide6" class="slide">
		<article>
			<h2>Your Support</h2>
			<h3>Is Greatly Appreciated</h3>

			<!-- <a href="#" class="btn black">Download our Case Statement</a>
			<a href="#" class="btn orange">Donate to the Champions Now</a> -->
		

			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				<?php the_content(); ?>
			<?php endwhile; endif; ?>

			<h4>Still Have Questions?</h4>
			<p>Call Us at <strong>(517) 432-4610</strong></p>
		</article>
	</section>

</div>

<script>
	//Parallax Bg Scroll
	$(window).scroll(function(e){
	  parallax();
	});
	function parallax(){
	  var scrolled = $(window).scrollTop();
	  $('img.wreath').css('top',-(scrolled*0.8)+'px');
	}


	//Campaign Goals (Raised Values)
	var goalraised = <?php echo $campaigngoal; ?>;
		facilityraised = <?php echo $facilities; ?>;
	    endowmentsrasied = <?php echo $endowments; ?>;
	    giftsraised = <?php echo $gifts; ?>;

	//Campaign Goals (Total Percentage)
	var goal = goalraised / 262 * 100;
	    facility = facilityraised / 92 * 100;
	    endowments = endowmentsrasied / 50 * 100;
	    gifts = giftsraised / 120 * 100;

	$(".progresscontainer.bar1 span.tooltip").css({"left":+goal+"%"});
	$(".progresscontainer.bar2 span.tooltip").css({"left":+facility+"%"});
	$(".progresscontainer.bar3 span.tooltip").css({"left":+endowments+"%"});
	$(".progresscontainer.bar4 span.tooltip").css({"left":+gifts+"%"});
</script>

<?php get_footer(); ?>
