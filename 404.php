<?php get_header(); ?>

<div id="pagecontent" class="pagecontent">

	<h2 class="lg"><?php _e('404 Error','html5reset'); ?></h2>
	<p>We're sorry, but the page you're requesting cannot be found. Please <a href="<?php echo home_url(); ?>">click here</a> to return to the home page and try again.</p>

</div>



<?php get_footer(); ?>






