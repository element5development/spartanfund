//Get User Agent String
var doc = document.documentElement;
doc.setAttribute('data-useragent', navigator.userAgent);

//Add Class based on Touch or NonTouch Device
if ("ontouchstart" in document.documentElement) {
	$('body').addClass('touch');
} else {
	$('body').addClass('no-touch');
}

//YouTube Z-Index Fix IE
$(document).ready(function () {
	$('iframe').each(function () {
		var url = $(this).attr("src");
		$(this).attr("src", url + "?wmode=transparent");
	});
});

//Responsive Images
$(document).ready(function ($) {
	$('.entry img').each(function () {
		$(this).removeAttr('width')
		$(this).removeAttr('height');
	});

	$('.wp-caption').each(function () {
		$(this).css("width", "100%");
	});

});

//Staff Members Sliding Bio
$(document).ready(function () {
	$('#staffmembers .btn').click(function (e) {
		e.preventDefault();
		$(this).closest('li').find('.bio').not(':animated').slideToggle();
	});
});

//Staff Members Sliding Bio
$(document).ready(function () {
	$('#donor .name').click(function (e) {
		e.preventDefault();
		$(this).closest('li').find('.info').not(':animated').slideToggle();
	});
});

//Staff Members Sliding Bio
$(document).ready(function () {
	$('#athlete .name').click(function (e) {
		e.preventDefault();
		$(this).closest('li').find('.info').not(':animated').slideToggle();
	});
});


//Remove Empty WP <p></p> tags
$('p').each(function () {
	var $this = $(this);
	if ($this.html().replace(/\s|&nbsp;/g, '').length == 0)
		$this.remove();
});


//Change Story ID's to Enable/Disable Parallax
$(document).ready(function () {
	$(window).resize(function () {
		if ($(window).width() < 1024) {
			$('#intro').attr('href', 'intro-mobile');
		}
	}).resize();
});


//Responsive Menu
jQuery(document).ready(function () {
	jQuery('header nav').meanmenu();
});

//Toggleclass for MenuBar
jQuery(document).ready(function () {
	$(".mean-container .mean-bar a").click(function () {
		$(".mean-container .mean-bar").toggleClass("orange");
		// $(".menubtn span").toggleClass("white"); 
		// $(".menubtn .menulabel").toggleClass("white"); 
	});
});

//Add Class to YouTube Embeds/iFrames
if ($('.entry iframe').length > 0) {
	$('.entry iframe').wrap("<div class='video-container'></div>");
}

$(window).resize(function () {
	$width = $(window).width(); // Width of the screen
	$height = $(window).height(); // Height of the screen
	$('.scroll').css({
		'bottom': $height + 'px'
	});
});

//Smooth Scroll
$(function () {
	$('a[href*=#]:not([href=#])').click(function () {
		if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
			if (target.length) {
				$('html,body').animate({
					scrollTop: target.offset().top
				}, 1000);
				return false;
			}
		}
	});
});