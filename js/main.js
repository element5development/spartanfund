jQuery(document).ready(function ($) {
	// browser window scroll (in pixels) after which the "back to top" link is shown
	var offset = 300,
		//browser window scroll (in pixels) after which the "back to top" link opacity is reduced
		offset_opacity = 1200,
		//duration of the top scrolling animation (in ms)
		scroll_top_duration = 700,
		//grab the "back to top" link
		$back_to_top = $('.cd-top');

	//hide or show the "back to top" link
	$(window).scroll(function () {
		($(this).scrollTop() > offset) ? $back_to_top.addClass('cd-is-visible'): $back_to_top.removeClass('cd-is-visible cd-fade-out');
		if ($(this).scrollTop() > offset_opacity) {
			$back_to_top.addClass('cd-fade-out');
		}
	});

	//smooth scroll to top
	$back_to_top.on('click', function (event) {
		event.preventDefault();
		$('body,html').animate({
			scrollTop: 0,
		}, scroll_top_duration);
	});

	// bind a click event to the 'skip' link
	$(".skip").click(function (event) {
		// strip the leading hash and declare
		// the content we're skipping to
		var skipTo = "#" + this.href.split('#')[1];
		// Setting 'tabindex' to -1 takes an element out of normal 
		// tab flow but allows it to be focused via javascript
		$(skipTo).attr('tabindex', -1).on('blur focusout', function () {
			// when focus leaves this element, 
			// remove the tabindex attribute
			$(this).removeAttr('tabindex');
		}).focus(); // focus on the content container
	});

	//DROPDOWN ACCESSIBILITY
	$('nav ul li>a').focus(function () {
		$(this).parent().siblings().removeClass('focus');
		$(this).parent().addClass('focus');
	});

	//HOME TITLE SWAP 
	(function () {
		var quotes = $(".keyword");
		var quoteIndex = -1;

		function showNextQuote() {
			++quoteIndex;
			quotes.eq(quoteIndex % quotes.length)
				.fadeIn(1000)
				.css("display", "table")
				.delay(1000)
				.fadeOut(1000, showNextQuote);
		}
		showNextQuote();
	})();

	/*----------------------------------------------------------------*\
		COOKIE CONSENT
		Note: Accepted tracking cookies should be enabled in Google Tag Manager
		https://www.grahamomaonaigh.com/gdpr-compliant-cookies-google-analytics/
	\*----------------------------------------------------------------*/
	// Available options located at https://github.com/ketanmistry/ihavecookies
	var options = {
    title: 'Cookies & Privacy',
		message: 'Cookies enable you to use shopping carts and to personalize your experience on our sites, tell us which parts of our websites people have visited, help us measure the effectiveness of ads and web searches, and give us insights into user behavior so we can improve our communications and products.',
		link: '/privacy-policy/',
		delay: 0,
		expires: 30, // 30 days
		moreInfoLabel: 'More information',
		acceptBtnLabel: 'Accept Cookies',
		advancedBtnLabel: 'Customize',
		cookieTypesTitle: 'Select cookies to accept',
		uncheckBoxes: false,
		// Optional callback function when 'Accept' button is clicked
		onAccept: function() {
			var myPreferences=$.fn.ihavecookies.cookie();
			console.log('The following cookie preferences have been saved:');
			console.log(myPreferences);
		},
		// Array of cookie types for which to show checkboxes.
		// - type: Type of cookie. This is also the label that is displayed.
		// - value: Value of the checkbox so it can be easily identified in your application.
		// - description: Description for this cookie type. Displayed in title attribute.
		cookieTypes: [
			{
				type: 'Analytics',
				value: 'analytics',
				description: 'Cookies related to site visits, browser types, etc.'
			},
			{
				type: 'Marketing',
				value: 'marketing',
				description: 'Cookies related to marketing, e.g. newsletters, social media, etc'
			}
		],
	}
	$('body').ihavecookies(options);
	// Reopen cookie consent
	$('button.cookie-open').click(function() {
    $('body').ihavecookies(options, 'reinit');
	});
	// Check if preference is selected
	if ($.fn.ihavecookies.preference('analytics') === true) {
		console.log('Analytics cookies have been accepted.');
	}
	if ($.fn.ihavecookies.preference('marketing') === true) {
		console.log('Marketing cookies have been accepted.');
	}
});