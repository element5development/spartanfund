<?php
/**
 * Template Name: Home Page
 */
 get_header(); ?>

<div class="header-wrapper">
	<header class="page-title" style="background-image: url(<?php the_field('title_background') ?>);">
		<div class="title-contain">
			<h1>
			<span><?php the_field('title_intro'); ?></span>
			<?php the_field('title'); ?>
			<?php if( have_rows('title_keywords') ): ?>
				<?php while ( have_rows('title_keywords') ) : the_row(); ?>
					<div class="keyword"><?php the_sub_field('keyword'); ?></div>
				<?php endwhile; ?>
			<?php endif; ?>
			</h1>
			<?php 
				$link = get_field('title_button');
				$link_url = $link['url'];
				$link_title = $link['title'];
				$link_target = $link['target'] ? $link['target'] : '_self';
			?>
			<a class="button_link" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
		</div>
	</header>
	<?php if ( get_field('annoucment_image') || get_field('annoucment_title') ) :?>
		<div class="banner">
			<?php if ( get_field('annoucment_image') ) : ?>
				<?php $image = get_field('annoucment_image'); ?>
				<img src="<?php echo esc_url($image['sizes']['large']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
			<?php endif; ?>
			<?php if ( get_field('annoucment_title') ) : ?>
				<h2><?php the_field('annoucment_title'); ?></h2>
			<?php endif; ?>
			<?php if ( get_field('annoucment_desciprtion') ) : ?>
				<p><?php the_field('annoucment_desciprtion'); ?></p>
			<?php endif; ?>
			<?php if ( get_field('button_one') ) : ?>
				<?php 
					$link = get_field('button_one');
					$link_url = $link['url'];
					$link_title = $link['title'];
					$link_target = $link['target'] ? $link['target'] : '_self';
				?>
				<a class="button" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
			<?php endif; ?>
			<?php if ( get_field('button_two') ) : ?>
				<?php 
					$link = get_field('button_two');
					$link_url = $link['url'];
					$link_title = $link['title'];
					$link_target = $link['target'] ? $link['target'] : '_self';
				?>
				<a class="button" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
			<?php endif; ?>
			<?php if ( get_field('button_three') ) : ?>
				<?php 
					$link = get_field('button_three');
					$link_url = $link['url'];
					$link_title = $link['title'];
					$link_target = $link['target'] ? $link['target'] : '_self';
				?>
				<a class="button" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
			<?php endif; ?>
		</div>
	<?php endif; ?>
</div>
<main id="pagecontent" class="pagecontent <?php echo $post->post_name;?>">
	<article>
		<?php if( have_rows('content_sections') ): ?>
			<?php while ( have_rows('content_sections') ) : the_row(); ?>
				<section class="text_and_image">
					<div class="text">
						<?php the_sub_field('cotents'); ?>
					</div>
					<figure>
						<?php $image = get_sub_field('image'); ?>
						<img src="<?php echo $image['sizes']['large'] ?>" alt="<?php echo $image['alt']; ?>" />
					</figure>
				</section>
			<?php endwhile; ?>
		<?php endif; ?>
		<section class="why-spartan-fund">
			<?php the_content(); ?>
		</section>
	</article>
</main>

<?php get_footer(); ?>