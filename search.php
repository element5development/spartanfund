<?php get_header(); ?>

<div class="pagecontent">

<div class="left">
	<!-- Display Parent Title -->
	<h2>Search</h2>
	<?php get_template_part('partials/sidenav'); ?>
</div>

<div id="pagecontent" class="right">
	<?php if (have_posts()) : ?>
		<h2><?php _e('Search Results','html5reset'); ?></h2>
		<?php while (have_posts()) : the_post(); ?>
			<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
				<h2><?php the_title(); ?></h2>
				<div class="entry">
					<?php the_excerpt(); ?>
				</div>
			</article>
		<?php endwhile; ?>
		<?php post_navigation(); ?>
	<?php else : ?>
		<h2><?php _e('Nothing Found','html5reset'); ?></h2>
	<?php endif; ?>
</div>

</div>



<?php get_footer(); ?>
