<?php get_header(); ?>

<?php $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 5600,1000 ), false, '' ); ?>

<?php if ( has_post_thumbnail() ) : ?>
	<header class="page-title has-background" style="background-image: url(<?php echo $src[0]; ?>);">
		<h1><?php the_title(); ?></h1>
	</header>
<?php else : ?>
	<header class="page-title">
		<h1><?php the_title(); ?></h1>
	</header>
<?php endif; ?>

<div class="pagecontent <?php echo $post->post_name;?>">

	<?php echo get_ancestor_tree(); ?>

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<article id="pagecontent" class="post">
				<?php the_content(); ?>
				<?php 
					if(is_page('our-staff')): get_template_part('partials/staff');
					elseif(is_page('donor-stories')): get_template_part('partials/donor-stories');
					elseif(is_page('our-champions')): get_template_part('partials/student-athletes');
					elseif(is_page( 154 )): get_template_part('partials/football-maps');
					elseif(is_page( 64 )): get_template_part('partials/mens-basketball-maps');
					elseif(is_page( 70 )): get_template_part('partials/hockey-maps');
					// elseif(is_page( 45 )): get_template_part('partials/opportunities');
					else : '';
				endif; ?>
		</article>
	<?php endwhile; endif; ?>

</div>



<?php get_footer(); ?>
