<?php

	// Options Framework (https://github.com/devinsays/options-framework-plugin)
	if ( !function_exists( 'optionsframework_init' ) ) {
		define( 'OPTIONS_FRAMEWORK_DIRECTORY', get_template_directory_uri() . '/inc/' );
		require_once dirname( __FILE__ ) . '/inc/options-framework.php';
	}

	// Theme Setup (based on twentythirteen: http://make.wordpress.org/core/tag/twentythirteen/)
	function html5reset_setup() {
		load_theme_textdomain( 'html5reset', get_template_directory() . '/languages' );
		add_theme_support( 'automatic-feed-links' );	
		add_theme_support( 'structured-post-formats', array( 'link', 'video' ) );
		add_theme_support( 'post-formats', array( 'aside', 'audio', 'chat', 'gallery', 'image', 'quote', 'status' ) );
		register_nav_menu( 'primary', __( 'Navigation Menu', 'html5reset' ) );
		add_theme_support( 'post-thumbnails' );
	}
	add_action( 'after_setup_theme', 'html5reset_setup' );
	
	// Scripts & Styles (based on twentythirteen: http://make.wordpress.org/core/tag/twentythirteen/)
	function html5reset_scripts_styles() {
		global $wp_styles;

		// Load Comments	
		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) )
			wp_enqueue_script( 'comment-reply' );
	
		// Load Stylesheets
//		wp_enqueue_style( 'html5reset-reset', get_template_directory_uri() . '/reset.css' );
//		wp_enqueue_style( 'html5reset-style', get_stylesheet_uri() );
	
		// Load IE Stylesheet.
//		wp_enqueue_style( 'html5reset-ie', get_template_directory_uri() . '/css/ie.css', array( 'html5reset-style' ), '20130213' );
//		$wp_styles->add_data( 'html5reset-ie', 'conditional', 'lt IE 9' );

		// Modernizr
		// This is an un-minified, complete version of Modernizr. Before you move to production, you should generate a custom build that only has the detects you need.
		// wp_enqueue_script( 'html5reset-modernizr', get_template_directory_uri() . '/js/modernizr-2.6.2.dev.js' );
		
	}
	add_action( 'wp_enqueue_scripts', 'html5reset_scripts_styles' );
	
	// WP Title (based on twentythirteen: http://make.wordpress.org/core/tag/twentythirteen/)
	function html5reset_wp_title( $title, $sep ) {
		global $paged, $page;
	
		if ( is_feed() )
			return $title;
	
//		 Add the site name.
		$title .= get_bloginfo( 'name' );
	
//		 Add the site description for the home/front page.
		$site_description = get_bloginfo( 'description', 'display' );
		if ( $site_description && ( is_home() || is_front_page() ) )
			$title = "$title $sep $site_description";
	
//		 Add a page number if necessary.
		if ( $paged >= 2 || $page >= 2 )
			$title = "$title $sep " . sprintf( __( 'Page %s', 'html5reset' ), max( $paged, $page ) );
//FIX
//		if (function_exists('is_tag') && is_tag()) {
//		   single_tag_title("Tag Archive for &quot;"); echo '&quot; - '; }
//		elseif (is_archive()) {
//		   wp_title(''); echo ' Archive - '; }
//		elseif (is_search()) {
//		   echo 'Search for &quot;'.wp_specialchars($s).'&quot; - '; }
//		elseif (!(is_404()) && (is_single()) || (is_page())) {
//		   wp_title(''); echo ' - '; }
//		elseif (is_404()) {
//		   echo 'Not Found - '; }
//		if (is_home()) {
//		   bloginfo('name'); echo ' - '; bloginfo('description'); }
//		else {
//		    bloginfo('name'); }
//		if ($paged>1) {
//		   echo ' - page '. $paged; }
	
		return $title;
	}
	add_filter( 'wp_title', 'html5reset_wp_title', 10, 2 );




//OLD STUFF BELOW


	// Load jQuery
	if ( !function_exists( 'core_mods' ) ) {
		function core_mods() {
			if ( !is_admin() ) {
				wp_deregister_script( 'jquery' );
				wp_register_script( 'jquery', ( "//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" ), false);
				wp_enqueue_script( 'jquery' );
			}
		}
		add_action( 'wp_enqueue_scripts', 'core_mods' );
	}

	// Clean up the <head>, if you so desire.
	//	function removeHeadLinks() {
	//    	remove_action('wp_head', 'rsd_link');
	//    	remove_action('wp_head', 'wlwmanifest_link');
	//    }
	//    add_action('init', 'removeHeadLinks');

	// Custom Menu
	register_nav_menu( 'primary', __( 'Navigation Menu', 'html5reset' ) );

	// Widgets
	if ( function_exists('register_sidebar' )) {
		function html5reset_widgets_init() {
			register_sidebar( array(
				'name'          => __( 'Sidebar Widgets', 'html5reset' ),
				'id'            => 'sidebar-primary',
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h3 class="widget-title">',
				'after_title'   => '</h3>',
			) );
		}
		add_action( 'widgets_init', 'html5reset_widgets_init' );
	}

	// Navigation - update coming from twentythirteen
	function post_navigation() {
		echo '<div class="navigation">';
		echo '	<div class="next-posts">'.get_next_posts_link('&laquo; Older Entries').'</div>';
		echo '	<div class="prev-posts">'.get_previous_posts_link('Newer Entries &raquo;').'</div>';
		echo '</div>';
	}

	// Posted On
	function posted_on() {
		printf( __( '<span class="sep">Posted </span><a href="%1$s" title="%2$s" rel="bookmark"><time class="entry-date" datetime="%3$s" pubdate>%4$s</time></a> by <span class="byline author vcard">%5$s</span>', '' ),
			esc_url( get_permalink() ),
			esc_attr( get_the_time() ),
			esc_attr( get_the_date( 'c' ) ),
			esc_html( get_the_date() ),
			esc_attr( get_the_author() )
		);
	}

	//Stop TinyMCE from removing span tags
	add_filter('tiny_mce_before_init', 'tinymce_init');

	function tinymce_init( $init ) {
	$init['extended_valid_elements'] .= ', span[style|id|nam|class|lang]';
	$init['verify_html'] = false;
	return $init;
	}

	//Customize TinyMCE Editor
		function my_theme_add_editor_styles() {
		add_editor_style( 'custom-editor-style.css' );
		}
		add_action( 'init', 'my_theme_add_editor_styles' );


	//Register Slides CPT
	function slide_post_type() {
	   
	   // Labels
		$labels = array(
			'name' => _x("Slides", "post type general name"),
			'singular_name' => _x("Slide", "post type singular name"),
			'menu_name' => 'Slides',
			'add_new' => _x("Add New", "slide item"),
			'add_new_item' => __("Add New Slide"),
			'edit_item' => __("Edit Slide"),
			'new_item' => __("New Slide"),
			'view_item' => __("View Slide"),
			'search_items' => __("Search Slides"),
			'not_found' =>  __("No Slides Found"),
			'not_found_in_trash' => __("No Slides Found in Trash"),
			'parent_item_colon' => ''
		);
		
		// Register post type
		register_post_type('slide' , array(
			'labels' => $labels,
			'public' => true,
			'has_archive' => false,
			'rewrite' => false,
			'menu_position' => 5,
			'supports' => array('title', 'editor', 'thumbnail'),		
 			'taxonomies' => array('category')
		) );
	}
	add_action( 'init', 'slide_post_type', 0 );

	//Register Staff CPT
	function staff_post_type() {
	   
	   // Labels
		$labels = array(
			'name' => _x("Staff", "post type general name"),
			'singular_name' => _x("Staff", "post type singular name"),
			'menu_name' => 'Staff Members',
			'add_new' => _x("Add New", "staff member"),
			'add_new_item' => __("Add New Staff Members"),
			'edit_item' => __("Edit Staff"),
			'new_item' => __("New Staff Member"),
			'view_item' => __("View Staff Member"),
			'search_items' => __("Search Staff Members"),
			'not_found' =>  __("No Staff Members Found"),
			'not_found_in_trash' => __("No Staff Members Found in Trash"),
			'parent_item_colon' => ''
		);
		
		// Register post type
		register_post_type('staff' , array(
			'labels' => $labels,
			'public' => true,
			'has_archive' => false,
			'rewrite' => false,
			'menu_position' => 5,
			'supports' => array('title', 'editor', 'thumbnail'),		
 			'taxonomies' => array('category')
		) );
	}
	add_action( 'init', 'staff_post_type', 0 );


	//Register Athletes CPT
	function athletes_post_type() {
	   
	   // Labels
		$labels = array(
			'name' => _x("Athlete", "post type general name"),
			'singular_name' => _x("Athlete", "post type singular name"),
			'menu_name' => 'Student Athletes',
			'add_new' => _x("Add New", "athlete"),
			'add_new_item' => __("Add New Athlete"),
			'edit_item' => __("Edit Athlete"),
			'new_item' => __("New Athlete"),
			'view_item' => __("View Athletes"),
			'search_items' => __("Search Athletes"),
			'not_found' =>  __("No Athletes Found"),
			'not_found_in_trash' => __("No Athletes Found in Trash"),
			'parent_item_colon' => ''
		);
		
		// Register post type
		register_post_type('athlete' , array(
			'labels' => $labels,
			'public' => true,
			'has_archive' => false,
			'rewrite' => false,
			'menu_position' => 5,
			'supports' => array('title', 'editor', 'thumbnail'),		
 			'taxonomies' => array('category')
		) );
	}
	add_action( 'init', 'athletes_post_type', 0 );


	//Register Donor Stories CPT
	function donor_post_type() {
	   
	   // Labels
		$labels = array(
			'name' => _x("Donor", "post type general name"),
			'singular_name' => _x("Donor", "post type singular name"),
			'menu_name' => 'Donor Stories',
			'add_new' => _x("Add New", "staff member"),
			'add_new_item' => __("Add New Donor Story"),
			'edit_item' => __("Edit Donor"),
			'new_item' => __("New Donor Story"),
			'view_item' => __("View Donor Stories"),
			'search_items' => __("Search Donor Stories"),
			'not_found' =>  __("No Donor Stories Found"),
			'not_found_in_trash' => __("No Donor Stories Found in Trash"),
			'parent_item_colon' => ''
		);
		
		// Register post type
		register_post_type('donor' , array(
			'labels' => $labels,
			'public' => true,
			'has_archive' => false,
			'rewrite' => false,
			'menu_position' => 5,
			'supports' => array('title', 'editor', 'thumbnail'),		
 			'taxonomies' => array('category')
		) );
	}
	add_action( 'init', 'donor_post_type', 0 );

	//Register Game Maps
	function custom_post_type() {
        //Maps
        $labels = array(
            'name'                => _x( 'Maps', 'Post Type General Name', 'twentysixteen' ),
            'singular_name'       => _x( 'Map', 'Post Type Singular Name', 'twentysixteen' ),
            'menu_name'           => __( 'Game Maps', 'twentysixteen' ),
            'parent_item_colon'   => __( 'Parent Map', 'twentysixteen' ),
            'all_items'           => __( 'All Maps', 'twentysixteen' ),
            'view_item'           => __( 'View Map', 'twentysixteen' ),
            'add_new_item'        => __( 'Add New Map', 'twentysixteen' ),
            'add_new'             => __( 'Add New', 'twentysixteen' ),
            'edit_item'           => __( 'Edit Map', 'twentysixteen' ),
            'update_item'         => __( 'Update Map', 'twentysixteen' ),
            'search_items'        => __( 'Search Map', 'twentysixteen' ),
            'not_found'           => __( 'Not Found', 'twentysixteen' ),
            'not_found_in_trash'  => __( 'Not Found in Trash', 'twentysixteen' ),
        );
        $args = array(
            'label'               => __( 'maps', 'twentysixteen' ),
            'description'         => __( 'Game Maps', 'twentysixteen' ),
            'labels'              => $labels,
            'supports'            => array( 'title', 'editor', 'thumbnail', 'custom-fields' ),
            'taxonomies'          => array( 'category' ),
            'public'              => true,
            'menu_position'       => 5,
            'has_archive'         => true,
        );
        register_post_type( 'Maps', $args );
        //Maps
        $labels = array(
            'name'                => _x( 'Opportunities', 'Post Type General Name', 'twentysixteen' ),
            'singular_name'       => _x( 'Opportunity', 'Post Type Singular Name', 'twentysixteen' ),
            'menu_name'           => __( 'Opportunities', 'twentysixteen' ),
            'parent_item_colon'   => __( 'Parent Opportunity', 'twentysixteen' ),
            'all_items'           => __( 'All Opportunities', 'twentysixteen' ),
            'view_item'           => __( 'View Opportunity', 'twentysixteen' ),
            'add_new_item'        => __( 'Add New Opportunity', 'twentysixteen' ),
            'add_new'             => __( 'Add New', 'twentysixteen' ),
            'edit_item'           => __( 'Edit Opportunity', 'twentysixteen' ),
            'update_item'         => __( 'Update Opportunity', 'twentysixteen' ),
            'search_items'        => __( 'Search Opportunities', 'twentysixteen' ),
            'not_found'           => __( 'Not Found', 'twentysixteen' ),
            'not_found_in_trash'  => __( 'Not Found in Trash', 'twentysixteen' ),
        );
        $args = array(
            'label'               => __( 'Opportunities', 'twentysixteen' ),
            'description'         => __( 'Opportunities', 'twentysixteen' ),
            'labels'              => $labels,
            'supports'            => array( 'title', 'editor', 'thumbnail', 'custom-fields' ),
            'taxonomies'          => array( 'categories' ),
            'public'              => true,
            'menu_position'       => 5,
            'has_archive'         => true,
        );
        register_post_type( 'Opportunities', $args );
    }
    add_action( 'init', 'custom_post_type', 0 );


	//Register Divider Shortcode
	function content_divider($atts, $content = null) {
	   return '<div class="divider">' . do_shortcode($content) . '</div>';
	}
	add_shortcode('divider', 'content_divider');

	//Register Column Shortcode
	function content_column($atts, $content = null) {
	   return '<div class="column">' . do_shortcode($content) . '</div>';
	}
	add_shortcode('col', 'content_column');

	//Register Box Shortcode
	function content_box($atts, $content = null) {
	   return '<div class="box">' . do_shortcode($content) . '</div>';
	}
	add_shortcode('box', 'content_box');


	//Register Contact Button Shortcode
    function contact_button( $atts, $content = null ) {
    extract(shortcode_atts(array(
    'url'	=> '',
    'target'	=> '',
    'color'	=> '',
    'size'	=> '',
    'align'	=> '',
    ), $atts));
 
	$style = ($color) ? ' '.$color. '' : '';
	$align = ($align) ? ' align'.$align : '';
	$size = ($size == 'large') ? ' large_button' : '';
	$target = ($target == '_blank') ? ' target="_blank"' : '';
 
	$out = '<div class="button_container"><a' .$target. ' class="button_link' .$style.$size.$align. '" href="' .$url. '"><span>' .do_shortcode($content). '</span></a></div>';
    
    return $out;
	}
	add_shortcode('button', 'contact_button');


	//Register Landing Page Button Shortcode
    function lp_button( $atts, $content = null ) {
    extract(shortcode_atts(array(
    'url'	=> '',
    'target'	=> '',
    'color'	=> '',
    'size'	=> '',
    'align'	=> '',
    'download'	=> '',
    ), $atts));
 
	$style = ($color) ? ' '.$color. '' : '';
	$align = ($align) ? ' align'.$align : '';
	$size = ($size == 'large') ? ' large_button' : '';
	$target = ($target == '_blank') ? ' target="_blank"' : '';
	$download = ($download == 'true') ? ' download' : '';
 
	$out = '<a' .$target.$download. ' class="btn' .$style.$size.$align. '" href="' .$url. '">' .do_shortcode($content). '</a>';
    
    return $out;
	}
	add_shortcode('lp_button', 'lp_button');



	//Add Auto Sub Menus Based on Custom WP Menu
	add_filter( 'wp_nav_menu_objects', 'my_wp_nav_menu_objects_sub_menu', 10, 2 );

	// filter_hook function to react on sub_menu flag
	function my_wp_nav_menu_objects_sub_menu( $sorted_menu_items, $args ) {
	if ( isset( $args->sub_menu ) ) {
	$root_id = 0;

	// find the current menu item
	foreach ( $sorted_menu_items as $menu_item ) {
	  if ( $menu_item->current ) {
	    // set the root id based on whether the current menu item has a parent or not
	    $root_id = ( $menu_item->menu_item_parent ) ? $menu_item->menu_item_parent : $menu_item->ID;
	    break;
	  }
	}

	// find the top level parent
	if ( ! isset( $args->direct_parent ) ) {
	  $prev_root_id = $root_id;
	  while ( $prev_root_id != 0 ) {
	    foreach ( $sorted_menu_items as $menu_item ) {
	      if ( $menu_item->ID == $prev_root_id ) {
	        $prev_root_id = $menu_item->menu_item_parent;
	        // don't set the root_id to 0 if we've reached the top of the menu
	        if ( $prev_root_id != 0 ) $root_id = $menu_item->menu_item_parent;
	        break;
	      } 
	    }
	  }
	}

	$menu_item_parents = array();
	foreach ( $sorted_menu_items as $key => $item ) {
	  // init menu_item_parents
	  if ( $item->ID == $root_id ) $menu_item_parents[] = $item->ID;

	  if ( in_array( $item->menu_item_parent, $menu_item_parents ) ) {
	    // part of sub-tree: keep!
	    $menu_item_parents[] = $item->ID;
	  } else if ( ! ( isset( $args->show_parent ) && in_array( $item->ID, $menu_item_parents ) ) ) {
	    // not part of sub-tree: away with it!
	    unset( $sorted_menu_items[$key] );
	  }
	}

	return $sorted_menu_items;
	} else {
	return $sorted_menu_items;
	}
	}


	//Breadcrumbs
	function the_breadcrumb() {
	    global $post;
		$title = get_the_title();
	    echo '<ul id="breadcrumbs">';
	    if (!is_home()) {
	        echo '<li><a href="';
	        echo get_option('home');
	        echo '">';
	        echo 'Home';
	        echo '</a></li><li class="separator"> &raquo; </li>';
	        if (is_category() || is_single()) {
	            echo '<li>';
	            the_category(' </li><li class="separator"> &raquo; </li><li> ');
	            if (is_single()) {
	                echo '</li><li class="separator"> &raquo; </li><li>';
	                the_title();
	                echo '</li>';
	            }
	        } elseif (is_page()) {
	            if($post->post_parent){
	                $anc = get_post_ancestors( $post->ID );
	                $title = get_the_title();
	                foreach ( $anc as $ancestor ) {
	                    $output = '<li><a href="'.get_permalink($ancestor).'" title="'.get_the_title($ancestor).'">'.get_the_title($ancestor).'</a></li> <li class="separator">&raquo;</li>' . $output;
	                }
	                echo $output;
	                echo '<li><strong title="'.$title.'"> '.$title.'</strong></li>';
	            } else {
	                echo '<li><strong> '.get_the_title().'</strong></li>';
	            }
	        }
	    }
	    elseif (is_tag()) {single_tag_title();}
	    elseif (is_day()) {echo"<li>Archive for "; the_time('F jS, Y'); echo'</li>';}
	    elseif (is_month()) {echo"<li>Archive for "; the_time('F, Y'); echo'</li>';}
	    elseif (is_year()) {echo"<li>Archive for "; the_time('Y'); echo'</li>';}
	    elseif (is_author()) {echo"<li>Author Archive"; echo'</li>';}
	    elseif (isset($_GET['paged']) && !empty($_GET['paged'])) {echo "<li>Blog Archives"; echo'</li>';}
	    elseif (is_search()) {echo"<li>Search Results"; echo'</li>';}
	    echo '</ul>';
	}



	// Post Thumbnails
	if ( function_exists( 'add_image_size' ) ) add_theme_support( 'post-thumbnails' );

	if ( function_exists( 'add_image_size' ) ) {
	add_image_size( 'post-thumb', 360, 275, true ); //Length, Width, Crop
	add_image_size( 'slide', 385, 205, true ); //Length, Width, Crop
	add_image_size( 'staff', 150, 9999); // Staff Thumb
	add_image_size( 'donor', 250, 250, true ); // Donor Thumb
	add_image_size( 'athlete', 250, 250, true); // Hard crop left top
}

function tribe_past_reverse_chronological ($post_object) {
	$past_ajax = (defined( 'DOING_AJAX' ) && DOING_AJAX && $_REQUEST['tribe_event_display'] === 'past') ? true : false;
	if(tribe_is_past() || $past_ajax) {
		$post_object = array_reverse($post_object);
	}
	return $post_object;
}
add_filter('the_posts', 'tribe_past_reverse_chronological', 100);

// FOOTER WIDGET AREAS
// Register custom sidebars
function footer_widget_areas() {
	$args = array(
		'name'          => __( 'footer-col-one' ),
		'before_widget' => '<div class="widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<p class="widget-title">',
		'after_title'   => '</p>',
	);
	register_sidebar($args);
	$args = array(
		'name'          => __( 'footer-col-two' ),
		'before_widget' => '<div class="widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<p class="widget-title">',
		'after_title'   => '</p>',
	);
	register_sidebar($args);
	$args = array(
		'name'          => __( 'footer-col-three' ),
		'before_widget' => '<div class="widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<p class="widget-title">',
		'after_title'   => '</p>',
	);
	register_sidebar($args);
	$args = array(
		'name'          => __( 'footer-col-four' ),
		'before_widget' => '<div class="widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<p class="widget-title">',
		'after_title'   => '</p>',
	);
	register_sidebar($args);
	$args = array(
		'name'          => __( 'footer-col-five' ),
		'before_widget' => '<div class="widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<p class="widget-title">',
		'after_title'   => '</p>',
	);
	register_sidebar($args);
	$args = array(
		'name'          => __( 'footer-col-six' ),
		'before_widget' => '<div class="widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<p class="widget-title">',
		'after_title'   => '</p>',
	);
	register_sidebar($args);
	$args = array(
		'name'          => __( 'footer-col-seven' ),
		'before_widget' => '<div class="widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<p class="widget-title">',
		'after_title'   => '</p>',
	);
	register_sidebar($args);
}
add_action( 'widgets_init', 'footer_widget_areas' );


// ANCESTOR TREE NAVIGATION
function get_ancestor_tree() {
	// Bail if this is not a page.
	if ( ! is_page() ) {
		return false;
	}
	// Get the current post.
	$post = get_post();
	//Get array of post ancestor IDs.
	$ancestors = get_post_ancestors( $post->ID );
	// If there are ancestors, get the top level parent. Otherwise use the current post's ID.
	$parent = ( ! empty( $ancestors ) ) ? array_pop( $ancestors ) : $post->ID;
	// Get all pages that are a child of $parent.
	$pages = get_pages( [
		'child_of' => $parent,
	] );
	// Bail if there are no results.
	if ( ! $pages ) {
			return false;
	}
	// Store array of page IDs to include latere on.
	$page_ids = array();
	foreach ( $pages as $page ) {
		$page_ids[] = $page->ID;
	}
	// Add parent page to beginning of $page_ids array.
	array_unshift( $page_ids, $parent );
	// Get the output and return results if they exist.
	$output = wp_list_pages( [
			'include'  => $page_ids,
			'title_li' => false,
			'echo'     => false,
	] );
	if ( ! $output ) {
		return false;
	} else { 
			return '<nav class="page-nav"><ul>' . PHP_EOL .
									$output . PHP_EOL .
							'</ul></nav>' . PHP_EOL;
	}
}

?>