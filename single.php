<?php get_header(); ?>

<div class="pagecontent">

<div class="left">
	<!-- Display Parent Title -->
	<h2><?php echo empty( $post->post_parent ) ? get_the_title( $post->ID ) : get_the_title( $post->post_parent );?></h2>
	<?php get_template_part('partials/sidenav'); ?>
</div>

<div id="pagecontent" class="right">
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	<article class="post" id="post-<?php the_ID(); ?>">
		<div class="entry">
			<?php the_content(); ?>
		</div>
	</article>
	<?php endwhile; endif; ?>
</div>

</div>



<?php get_footer(); ?>
