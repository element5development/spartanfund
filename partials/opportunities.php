<ul id="athlete">
<?php //Opportunities
query_posts( array( 'post_type' => 'opportunities', 'posts_per_page' => -1, 'order' => ASC) );
if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<li class="post-<?php the_ID(); ?>">

	<article>
		<h3 class="entry-header">
			<a href="#" class="name"><?php the_title(); ?></a>
		</h3>
		<div class="info">
			<div class="post-feed-content"><?php the_content(); ?></div>
		</div>
	</article>
	
</li>
<?php endwhile; endif; wp_reset_query(); ?>
</ul>