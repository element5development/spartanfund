<ul id="staffmembers">
<?php //Staff Query
	query_posts( array( 'post_type' => 'staff', 'posts_per_page' => -1, 'order' => ASC) );
	if ( have_posts() ) : while ( have_posts() ) : the_post(); 
?>
<li class="post-<?php the_ID(); ?>">
	
	<figure>
		<?php if ( has_post_thumbnail() ) {
			the_post_thumbnail('medium');
		} else { ?>
			<img src="<?php bloginfo('template_directory'); ?>/images/default-staff.jpg" alt="<?php the_title(); ?>" />
		<?php } ?>
	</figure>

	<article>
		
		<h3><?php the_title(); ?></h3>

		<?php if( get_field('title') ): ?>
			<p><strong><?php the_field('title'); ?></strong></p>
		<?php endif; ?>

		<?php if( get_field('email') ): ?>
			<p><a href="mailto:<?php the_field('email'); ?>"><?php the_field('email'); ?></a></p>
		<?php endif; ?>

		<?php
			$cc = get_the_content();
			if($cc != '') { 
		?>
			<a href="#" class="btn">Read Full Bio &raquo;</a>
			<div class="bio">
				<?php the_content(); ?>
			</div>
		<?php } else { ?>
		<?php } ?>

	</article>
</li>
<?php endwhile; endif; wp_reset_query(); ?>
</ul>