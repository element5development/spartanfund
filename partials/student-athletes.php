<ul id="athlete">
<?php //Student Athletes
query_posts( array( 'post_type' => 'athlete', 'posts_per_page' => -1, 'order' => ASC) );
if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<li class="post-<?php the_ID(); ?>">

	<article>
		
		<h3><a href="#" class="name"><?php the_title(); ?></a></h3>

		<div class="info">

		<div class="athlete-intro">
		<div class="athletes">
		<?php if( get_field('quote') ): ?>
			<blockquote><?php the_field('quote'); ?>
			<cite><?php the_title(); ?>
			<?php if( get_field('graduation_year') ): ?>,&nbsp;<?php the_field('graduation_year'); ?>
			<?php endif; ?></cite>
			</blockquote>
		<?php endif; ?>

		</div>
			<figure>
			<?php if ( has_post_thumbnail() ) { 
	        	the_post_thumbnail('athlete'); } ?>
			</figure>
		</div>	

		<!-- Sport -->
		<?php if( get_field('sport') ): ?>
			<div class="divider">
				<p class="label">Sport:</p>
				<p><?php the_field('sport'); ?></p>
			</div>
		<?php endif; ?>

		<!-- Major -->
		<?php if( get_field('major') ): ?>
			<div class="divider">
				<p class="label">Major:</p>
				<p><?php the_field('major'); ?></p>
			</div>
		<?php endif; ?>

		<!-- Position -->
		<?php if( get_field('position') ): ?>
			<div class="divider">
				<p class="label">Position:</p>
				<p><?php the_field('position'); ?></p>
			</div>
		<?php endif; ?>

		<!-- Hometown -->
		<?php if( get_field('hometown') ): ?>
			<div class="divider">
				<p class="label">Hometown:</p>
				<p><?php the_field('hometown'); ?></p>
			</div>
		<?php endif; ?>

		<!-- Record Achievements  -->
		<?php if( get_field('record_achievements') ): ?>
			<div class="divider">
				<p class="label">Record Achievements:</p>
				<p><?php the_field('record_achievements'); ?></p>
			</div>
		<?php endif; ?>

		<!-- Video  -->
		<?php if( get_field('video') ): ?>
			<div class="divider">
				<p class="label">Video:</p>
				<p><?php the_field('video'); ?></p>
			</div>
		<?php endif; ?>

		</div>

	</article>
</li>
<?php endwhile; endif; wp_reset_query(); ?>
</ul>