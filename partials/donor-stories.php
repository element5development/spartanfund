<ul id="donor">
<?php //Donor Stories
query_posts( array( 'post_type' => 'donor', 'posts_per_page' => -1, 'order' => ASC) );
if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<li class="post-<?php the_ID(); ?>">

	<article>
		
		<h3><a href="#" class="name"><?php the_title(); ?></a></h3>

		<div class="info">

		<div class="donor-intro">
		<div class="donors">
		<?php if( get_field('philanthropic_support_to_msu') ): ?>
		<div class="divider">
			<blockquote>
				<?php the_field('philanthropic_support_to_msu'); ?>
				<cite>- <?php the_title(); ?></cite>
			</blockquote>
		</div>
		<?php endif; ?>
		</div>
			<figure>
			<?php if ( has_post_thumbnail() ) { 
	        	the_post_thumbnail('donor'); } ?>
			</figure>
		</div>	

		<!-- City/Town -->
		<?php if( get_field('city/town_of_residence') ): ?>
			<div class="divider">
				<p class="label">City/Town of Residence:</p>
				<p><?php the_field('city/town_of_residence'); ?></p>
			</div>
		<?php endif; ?>

		<!-- Member Since -->
		<?php if( get_field('spartan_member_since') ): ?>
		<div class="divider">
			<p class="label">Spartan Member Since:</p>
			<p><?php the_field('spartan_member_since'); ?></p>
		</div>
		<?php endif; ?>

		<!-- Education -->
		<?php if( get_field('education') ): ?>
		<div class="divider">
			<p class="label">Education:</p>
			<p><?php the_field('education'); ?></p>
		</div>
		<?php endif; ?>

		<!-- Sports Played -->
		<?php if( get_field('sports_played_at_msu') ): ?>
		<div class="divider">
			<p class="label">Sports Played at MSU:</p>
			<p><?php the_field('sports_played_at_msu'); ?></p>
		</div>
		<?php endif; ?>

		<!-- Career -->
		<?php if( get_field('career') ): ?>
		<div class="divider">
			<p class="label">Career:</p>
			<p><?php the_field('career'); ?></p>
		</div>
		<?php endif; ?>

		<!-- Spartan Fund Involvement -->
		<?php if( get_field('spartan_fund_invovlement') ): ?>
		<div class="divider">
			<p class="label">Spartan Fund Involvement:</p>
			<p><?php the_field('spartan_fund_invovlement'); ?></p>
		</div>
		<?php endif; ?>

		<!-- Season Ticket Holder Since -->
		<?php if( get_field('season_ticket_holder_since') ): ?>
		<div class="divider">
			<p class="label">Season Ticket Holder Since:</p>
			<p><?php the_field('season_ticket_holder_since'); ?></p>
		</div>
		<?php endif; ?>

		<!-- Fondest MSU Memory -->
		<?php if( get_field('fondest_msu_memory') ): ?>
		<div class="divider">
			<p class="label">Fondest MSU Memory:</p>
			<p><?php the_field('fondest_msu_memory'); ?></p>
		</div>
		<?php endif; ?>


		</div>

	</article>
</li>
<?php endwhile; endif; wp_reset_query(); ?>
</ul>